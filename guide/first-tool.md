# 开发第一个轻工具

如果你有前端开发基础，那么使用我们的开发模板来开发一个轻工具是很简单的。

Let's go

## 开发目标

本节的目标是分析一下开发模板源码，看看一个**能为用户输入的任意文本生成md5摘要**的轻工具是如何实现的。

## 克隆模板仓库

轻工具模板仓库[在此](https://gitee.com/zhangkefei/aib-light-tool-template)，`git clone` 克隆它。

## 安装依赖

跟其他所有前端项目一样，进入目录： `cd aib-light-tool-template`

安装依赖：`npm i`

## 运行一下试试

运行 `npm run dev`，打开 `http://localhost:5173`，如果你看到如下画面，说明一切正常。
![first-tool-1](/images/first-tool-1.jpg)

:::tip 注意
开发环境中，提示词会自动触发。
:::

模板仓库已经实现从文本生成md5的功能了。根据提示，输入文本看看效果！

上面的功能是如何实现的呢？我们来看看 `package.json` 和 `lib/main.js` 这两个文件。
## `package.json`

打开仓库根目录下的 `package.json` 文件，它跟通常的npm包的`package.json`几乎一样，只是多了一个 `assistantInBrowser` 字段配置。[详细说明请看这里](/tools/package.md)。

在此示例中，我们只需要了解 `assistantInBrowser.phrase` 是**用户用来唤起轻工具的唤醒词**，以及 `assistantInBrowser.input.required` 配置：
- `message` 字段是**你向用户询问参数时的提示语**
- `type` 字段是**要求用户输入的参数类型**

## `lib/main.js`

打开 `lib/main.js`，是不是很熟悉？它导出了一个函数，入参就是你在 `package.json` 中配置的入参，它返回一个值，就是你最终给用户的结果。

其他与平时开发方式完全一样。结束。

:::tip 提示
想为用户提供更加丰富的交互，请看我们的[API文档](/tools/api.md)
:::

:::tip 注意
为保持简单，目录函数的返回值只支持 `string` 类型与 `file` 类型

`file` 类型对应浏览器的 `File` 接口，具体参见[File - Web API 接口参考 | MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/File)
:::

## 开发你自己的轻工具

1. 修改`package.json`
2. 可选：修改`README.md`
3. 在 `lib/main.js` 中写入逻辑，最终需要导出一个函数
4. **构建：** 在开发环境中测试无误后，运行 `npm run build` 构建生产版本
5. **测试：** 可以使用 `npm run staging` 命令进行发布前测试，此时使用的代码就是构建后的 `dist` 目录
6. **发布：** 前往 bitones.cn -> 开发者 -> 发布应用，选择构建后的dist目录，发布应用。
7. 大功告成！现在可以到 bitones.cn/chat 页面，输入 ~ 后加**唤醒词**来使用了！