---
title: 为什么用 AIB
---
# 为什么用 AIB {#why-aib}

## 现实问题 {#the-problems}

当前，生成式AI解决了很多创意生产的问题，但是还有很多问题没有解决。比如帮助人们处理文件。现在AI有了一颗聪明的大脑，但是还没有手和脚。我们致力于提供一个不仅大脑聪明，而且手脚灵活的AI助理。

## 为什么 {#why}

在工作和生活中生成式AI能解决的问题，占一小部分。还有很多其他问题是需要我们动手去操作的。这部分问题是目前生成是AI解决不了的。而我们要解决这类问题，需要去下载各种各样的软件，这些软件有很多是基于系统原生开发的，权限很高，这就需要人们对其仔细甄别，否则很容易被病毒入侵或遭到隐私泄露。我们现在将这部分应用全部整合到浏览器里，利用浏览器的安全策略来保证用户的隐私不被泄露、应用的权限得到管控，同时能够很好地完成用户的任务。

身怀绝技的开发者可以上传自己的应用作品，来解决各种各样的问题。具体的开发方式可以参照我们的文档。开发者上传应用，可以为用户解决问题，同时还可以获得相应的报酬。

## 加入我们 {#join-us}

如果你认同我们所做的事情，可以用以下方式支持这件事：

- 使用 AIB 开发和发布软件，让 AIB 壮大的同时获得收益
- 加入 AIB 平台的开发，使其更加完善

<!-- <small class="cn-footnote">
<br/>
<strong class="title">译者注</strong>
<a id="footnote-1"></a>[1] 暂以意译方式呈现。
</small> -->
