# 轻工具

轻工具是一种直接通过与AIB进行对话交互即可完成的轻量化的功能。

轻工具其实也是一个npm包，它是npm包规范的超集，定义了一些扩展字段。

一个轻工具包至少包含三个文件：`package.json`, `main.js`, `README.md`

- `package.json` 除了遵循npm包规范外，还定义了入参以及其他有用的信息。[查看详情](package.md)
- `main.js` 是功能函数的定义位置，查看详情。[查看详情](api.md)
- `README.md` 应该包含使用帮助信息，供用户查看。[查看详情](api.md)