# API

## 运行环境

轻工具功能全部由 `main.js` 完成。为保证页面不被恶意程序篡改，`main.js` 被限制在 WebWorker 中运行。有关 WebWorker 的信息，请参考 [MDN页面](https://developer.mozilla.org/zh-CN/docs/Web/API/Web_Workers_API)。

## `main.js`文件

`main.js` 文件位于开发模板仓库的`lib`目录下，它默认导出一个函数，签名如下：
```js
(required: (string|File)[], options: {}) => string|File
```
举例说明，它的入参可能有如下情况：
```js
(param: string) => string|File
(param: File) => string|File
(param1: string, param2: File, ...) => string|File
(param1: string, param2: File, ..., {option1: string, option2: boolean, ...}) => string|File
```
以上，除`options`对象中定义的可选参数外，前面的params均为必填参数。

### 参数定义方式

- `params`必填参数，由`package.json`文件中的`assistantInBrowser.input.required`字段定义
- `options`可选参数，由`package.json`文件中的`assistantInBrowser.input.options`字段定义
- 函数返回参数，由`package.json`文件中的`assistantInBrowser.output`字段定义

### 完整示例

**开发模板中的示例文件**
```js
import md5 from "md5";

export default (str) => {
    try {
        return md5(str)
    } catch (error) {
        console.error(error);
        $.message('error')
    }
}
```

## Worker 环境中支持的 API

前端说到，`main.js` 文件是运行在 Worker 环境中的。因此，它没有操作DOM的能力，Worker 环境中支持的API列表可参考 [MDN - Web Worker 支持的 Web API](https://developer.mozilla.org/zh-CN/docs/Web/API/Web_Workers_API#%E6%94%AF%E6%8C%81%E7%9A%84_web_api)。

## 交互对象 `$`
你可能注意到了，上面 `main.js` 示例中，出现了 `$` 符号。这是AIB平台提供的一个交互对象，它包含常用的对话交互方法。通过它，开发者可以发起对话交互。

### `$.message`
**类型：** `(msg: string) => void`

在对话中展示一段文本信息

### `$.warning`
**类型：** `(msg: string) => void`

在对话中展示一段警告信息。一般用于可以完成任务，但是需要提醒用户注意的场景。

### `$.error`
**类型：** `(msg: string) => void`

在对话中展示一段错误信息。一般用于因某些原因不能完成任务并提示用户的场景。

### `$.processing`
**类型：** `async () => ProcessingObject`

在对话中展示**进行中**状态。用于执行耗时任务时提示用户，缓解焦虑。类似于loading。

### `$.complete`
**类型：** `async (p: ProcessingObject) => void`

用于结束进行中状态。入参为 `$.processing` 返回的 `ProcessingObject`。

### `$.prompt`
**类型：** `async (msg: string) => string`

向用户请求一段文本，msg参数会展示在对话记录中，开发者会得到用户输入值。

### `$.file`
**类型：** `async (msg: string) => File`

向用户请求一个文件，msg参数会展示在对话记录中，开发者会得到用户选择的文件。

### `$.dirhandle`
**类型：** `async (msg: string) => FileSystemDirectoryHandle`

向用户请求一个`FileSystemDirectoryHandle`对象，开发者可使用此对象操作目录。msg参数会展示在对话记录中。

`FileSystemDirectoryHandle`相关文档，请参见[MDN页面](https://developer.mozilla.org/zh-CN/docs/Web/API/FileSystemDirectoryHandle)。

### `$.alert`
**类型：** `async (msg: string) => void`

向用户展示**阻塞的**警告信息，msg参数会展示在对话记录中。

:::tip 提示
与`$.warning`不同的是，`$.alert`用于需要对用户进行强提醒，用户确认后才能继续执行的场景。
:::

### `$.confirm`
**类型：** `async (msg: string) => true | false`

向用户展示需要确认的信息，msg参数会展示在对话记录中。返回值`true`代表用户确认，`false`代表用户取消。开发者需要根据返回值决定是否继续执行。

### `$.playform`
**类型：** `async () => PlatformInfo`

获取当前运行环境信息。返回的`PlatformInfo`形式如下：
```js
{
    name: "chrome",     // 浏览器名称
    version: "118.0.0", // 浏览器版本
    os: "Mac OS",       // 操作系统名称
    type: "browser",    // 平台类型，总是"browser"
    worker: true        // 是否worker环境
}
```

### `$.result`
**类型：** `(data: string | File) => void`

向用户返回数据。与`$.message`的区别是，此方法展示的文本数据会自带一个复制按钮，展示的文件数据会自带一个下载按钮。

:::tip 后续功能
平台后续会针对该方法返回的文件类型，提供一些便捷的能力。例如，如果返回图片文件，会自动在对话记录中预览，而不是只显示一个文件消息。
:::

:::tip 提示
如果`main.js`文件没有中途向用户返回数据的需求，则可在执行结束时，直接`return`数据或文件。平台会自动调用`$.result`，这样更符合编程习惯。
:::

## 工具集合 `_`

就是它，`Lodash`，Worker环境直接加载了该工具库，你可以使用`_.方法名`直接调用。使用方法详见[Lodash文档](https://www.lodashjs.com/)。