# package.json

package.json定义一个轻工具的基础信息。下面是必要字段的详细要求：
## name
- **类型：** `string`
- **必需：** 是

轻工具包的名字。只能包含字母、数字、中划线（减号）。
## version
- **类型：** `string`
- **必需：** 是

轻工具包的版本。只能是x.y.z的格式，例如1.0.0。

:::tip 注意
轻工具包的名字和版本，共同组成了轻工具包的id。这个id是唯一的，不可重复。
:::

## description
- **类型：** `string`
- **必需：** 是

轻工具包的描述。需要简单描述该轻工具包完成的工作内容。这个描述会作为**系统信息**在用户调起此工具的时候显示在对话记录中。
:::tip 提示
轻工具包的详细功能信息可以放在README.md文件里。
:::
## assistantInBrowser
- **类型：** `object`
- **必需：** 是

AIB对npm规范的扩展字段，包含的所有AIB特有的字段。
## assistantInBrowser.phrase
- **类型：** `string`
- **必需：** 是

轻工具的唤醒词。用户通过输入唤醒词来调起轻工具，因此应该简短并且口语化，方便用户唤起和记忆。
## assistantInBrowser.input
- **类型：** `object`
- **必需：** 是

功能函数的入参。
## assistantInBrowser.input.required[]
- **类型：** `object[]`
- **必需：** 否

入参中的必填参数。这是一个列表，里面包含了一个个参数。平台会依次向用户请求输入数据，并在调用功能函数时传入。
## assistantInBrowser.input.required[].message
- **类型：** `string`
- **必需：** 是

向用户请求数据时的提示语。
## assistantInBrowser.input.required[].type
- **类型：** `'string' | 'file'`
- **必需：** 是

参数的类型。只能是`string`或`file`其中之一。
## assistantInBrowser.input.options
- **类型：** `object`
- **必需：** 否

入参中的非必填参数。在调起时，平台不会向用户请求这些参数，并使用默认值来调用功能函数。但是，平台会提供一个定制功能，用户在需要更改这些参数时，通过定制功能即可完成。
## assistantInBrowser.output
- **类型：** `object`
- **必需：** 是

功能函数的返回值。
## assistantInBrowser.output.type
- **类型：** `'string' | 'file'`
- **必需：** 是

返回值类型，只能是`string`或`file`其中之一。

**完整示例**
```json
{
    "name": "file2md5",
    "version": "1.0.0",
    "description": "为一个文件生成md5摘要",
    "assistantInBrowser": {
        "phrase": "从文件生成md5",
        "input": {
            "required": [
                {
                    "message": "请选择一个文件",
                    "type": "file"
                }
                ...
            ],
            "option": {
                "length": {
                    "type": "number",
                    "default": 16
                }
                ...
            }
        },
        "output": {
            "type": "string"
        }
    }
}
```