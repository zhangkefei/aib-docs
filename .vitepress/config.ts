import { defineConfig } from 'vitepress'
// import renderPermaLink from './render-perma-link'
// import MarkDownItCustomAnchor from './markdown-it-custom-anchor'

const ogDescription = '阿布，您的专属AI助理'
const ogImage = ''
const ogTitle = 'AIB'
const ogUrl = ''

export default defineConfig({
  title: 'AIB 官方开发文档',
  description: '',
  lang: 'zh-CN',
  base: '/docs/',

  head: [
    ['link', { rel: 'icon', type: 'image/svg+xml', href: './diamond.svg' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:title', content: ogTitle }],
    ['meta', { property: 'og:image', content: ogImage }],
    ['meta', { property: 'og:url', content: ogUrl }]
  ],

  vue: {
    reactivityTransform: true
  },

  locales: {
    root: { label: '简体中文' },
    en: { label: 'English', link: '/' },
  },

  themeConfig: {
    logo: '/diamond.svg',

    outline: {
      label: '本页目录'
    },

    footer: {
      message: 'AIB（阿布），您的专属AI助理',
      copyright: 'Copyright © 2023 bitones'
    },

    nav: [
      { text: '首页', link: '/', activeMatch: '' },
      { text: '指引', link: '/guide/', activeMatch: '/guide/' },
      { text: '轻工具', link: '/tools/', activeMatch: '/tools/' },
      { text: '轻应用', link: '/apps/', activeMatch: '/apps/' },
      {
        text: '相关链接',
        items: [
          {
            text: 'Awesome AIB',
            link: 'https://'
          },
          {
            text: '更新日志',
            link: 'https://'
          }
        ]
      },
    ],
    sidebar: {
      '/guide/': [
        {
          text: '指引',
          items: [
            {
              text: '为什么用 AIB',
              link: '/guide/why'
            },
            {
              text: '开始',
              link: '/guide/'
            },
            {
              text: '应用介绍',
              link: '/guide/introduce'
            },
            {
              text: '第一个轻工具',
              link: '/guide/first-tool'
            },
            {
              text: '第一个轻应用',
              link: '/guide/first-app'
            },
            {
              text: '理念',
              link: '/guide/philosophy'
            }
          ]
        },
      ],
      '/tools/': [
        {
          text: '轻工具',
          items: [
            {
              text: '介绍',
              link: '/tools/'
            },
            {
              text: 'package.json',
              link: '/tools/package'
            },
            {
              text: 'API',
              link: '/tools/api'
            }
          ]
        },
        {
          text: '专题',
          items: [
            {
              text: '在WebWorker中处理图像',
              link: '/tools/topic-image-process-in-worker'
            },
            {
              text: '在WebWorker中处理音频',
            },
          ]
        }
      ],
      '/apps/': [
        {
          text: '轻应用',
          items: [
            {
              text: '介绍',
              link: '/apps/'
            },
            {
              text: 'API',
              link: '/apps/api'
            }
          ]
        },
      ]
    }
  }
})
