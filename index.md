---
layout: home

title: AIB
titleTemplate: 您的专属AI助理

hero:
  name: AIB，阿布
  text: AssistantInBrowser
  tagline: 您的专属AI助理
  image:
    src: ./logo-with-shadow.png
    alt: AIB
  actions:
    - theme: brand
      text: 开始
      link: /guide/
    - theme: alt
      text: 为什么用 AIB？
      link: /guide/why

features:
  - icon: 💡
    title: 融合创造力
    details: 融合所有生成式AI的创造力为你所用
  - icon: ⚡️
    title: 交互式AI
    details: 通过智能交互对话，大幅拓展AI能力
  - icon: 🛠️
    title: 安全至上
    details: 充分利用Web安全策略，免除隐私泄漏和病毒烦恼
  - icon: 📦
    title: 用户友好
    details: 通过集合账户、自然交互完成任务，零学习成本
  - icon: 🔩
    title: 开发者友好
    details: 向所有开发者开放，发布应用并收获报酬
  - icon: 🔑
    title: 全面智能
    details: 致力于打造拥有全面AI能力的智能助理
---
